package server

import (
	"github.com/labstack/echo"
	"net/http"
)

func userRoutes() {
	authGroup := Server.Group("/auth")
	userGroup := Server.Group("/user")

	authGroup.POST("/login", Login)
	authGroup.POST("/register", Register)

	userGroup.GET("/profile", GetProfile)
	userGroup.POST("/profile", UpdateProfile)
	userGroup.DELETE("/profile", DeleteProfile)
}

func Login(c echo.Context) error {
	return c.String(http.StatusOK, "Hello")
}

func Register(c echo.Context) error {
	return c.String(http.StatusOK, "Hello")
}

func GetProfile(c echo.Context) error {
	return c.String(http.StatusOK, "Hello")
}

func UpdateProfile(c echo.Context) error {
	return c.String(http.StatusOK, "Hello")
}

func DeleteProfile(c echo.Context) error {
	return c.String(http.StatusOK, "Hello")
}

func GetUser(c echo.Context) error {
	return c.String(http.StatusOK, "Hello")
}
