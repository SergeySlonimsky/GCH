package main

import "GCH/backend/config"

func main() {
	config.InitConfig()
	config.RunServer()
}
