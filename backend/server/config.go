package server

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"os"
)

var Server = echo.New()

func createServerConfig() {
	Server.Use(middleware.Logger())
	Server.Use(middleware.Recover())
	Server.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*"},
		AllowHeaders:     []string{"authorization", "Content-Type"},
		AllowCredentials: true,
		AllowMethods:     []string{echo.OPTIONS, echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))
}

func RunServer() {
	createServerConfig()
	handleRoutes()
	Server.Logger.Fatal(Server.Start(":" + os.Getenv("SERVER_PORT")))
}

func handleRoutes() {
	userRoutes()
	lotRoutes()
}
