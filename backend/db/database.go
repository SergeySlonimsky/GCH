package db

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"log"
	"os"
)

var Db *sqlx.DB

func connectDatabase() {
	mysql_db, mysql_err := sqlx.Open("mysql", getConnectionString())

	if mysql_err != nil {
		log.Println("Database error", mysql_err)
	}

	Db = mysql_db
}

func getConnectionString() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s",
		os.Getenv("MYSQL_USER"),
		os.Getenv("MYSQL_PASSWORD"),
		os.Getenv("MYSQL_HOST"),
		os.Getenv("MYSQL_PORT"),
		os.Getenv("MYSQL_DATABASE"))
}
